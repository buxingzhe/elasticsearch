package com.example.es;

import co.elastic.clients.elasticsearch.core.BulkResponse;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.bulk.BulkResponseItem;
import com.alibaba.fastjson.JSON;
import com.example.es.entity.UserVo;
import com.example.es.service.ESDocumentService;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 * 业务操作测试
 * @author hulei
 * @date 2023/8/26 18:35
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class ESDocumentTest {
    private final static String INDEX_NAME = "db_api_idx_uservo";

    @Autowired
    private ESDocumentService documentDemoService;


    @Test
    public void testCreateByFluentDSL() throws Exception {
        // 构建文档数据
        UserVo userVO = new UserVo();
        userVO.setId(1L);
        userVO.setUserName("赵云2");
        userVO.setAge(11);
        userVO.setCreateTime(new Date());
        userVO.setUpdateTime(new Date());
        userVO.setEmail("ss.com");
        userVO.setVersion(1);
        userVO.setHeight(12D);

        // 新增一个文档
        IndexResponse response = documentDemoService.createByFluentDSL(INDEX_NAME, userVO.getId().toString(), userVO);
        System.out.println("response.forcedRefresh() -> " + response.forcedRefresh());
        System.out.println("response.toString() -> " + response);
    }

    /**
     * BuilderPattern 模式创建
     */
    @Test
    public void testCreateByBuilderPattern() throws Exception {
        // 构建文档数据
        UserVo userVO = new UserVo();
        userVO.setId(2L);
        userVO.setUserName("赵云2");
        userVO.setAge(12);
        userVO.setCreateTime(new Date());
        userVO.setUpdateTime(new Date());
        userVO.setEmail("ss.com");
        userVO.setVersion(1);
        userVO.setHeight(12D);

        // 新增一个文档
        IndexResponse response = documentDemoService.createByBuilderPattern(INDEX_NAME, userVO.getId().toString(), userVO);
        System.out.println("response.toString() -> " + response.toString());
    }

    /**
     * json格式创建
     */
    @Test
    public void testCreateByJSON() throws Exception {
        // 构建文档数据
        UserVo userVO = new UserVo();
        userVO.setId(3L);
        userVO.setUserName("赵云3");
        userVO.setAge(13);
        userVO.setCreateTime(new Date());
        userVO.setUpdateTime(new Date());
        userVO.setEmail("ss.com");
        userVO.setVersion(1);
        userVO.setHeight(12D);

        // 新增一个文档
        IndexResponse response = documentDemoService.createByJson(INDEX_NAME, userVO.getId().toString(), JSON.toJSONString(userVO));

        System.out.println("response.toString() -> " + response.toString());
    }

    /**
     * 异步创建文档
     */
    @Test
    public void testCreateAsync() {
        // 构建文档数据
        UserVo userVO = new UserVo();
        userVO.setId(4L);
        userVO.setUserName("赵云4");
        userVO.setAge(14);
        userVO.setCreateTime(new Date());
        userVO.setUpdateTime(new Date());
        userVO.setEmail("ss.com");
        userVO.setVersion(1);
        userVO.setHeight(12D);

        documentDemoService.createAsync(INDEX_NAME, userVO.getId().toString(), userVO, (indexResponse, throwable) -> {
            // throwable必须为空
            Assertions.assertNull(throwable);
            // 验证结果
            System.out.println("response.toString() -> " + indexResponse.toString());
        });
    }

    /**
     * 批量新增数据(这里模拟插入了一亿条数据,好测试数据查询速度)
     */
    @Test
    public void testBulkCreate() {
        List<CompletableFuture<Void>> futureList = new ArrayList<>();
        int start = 39500000;
        for (int j = 1; j<=15 ; j++){
            // 构造文档集合
            int finalJ = j;
            int finalJ1 = j;
            CompletableFuture<Void> future = CompletableFuture.runAsync(()->{
                List<Object> list = new ArrayList<>();
                for (int i = start+500000*(finalJ1 -1); i < start+(500000 * finalJ); i++) {
                    UserVo userVO = new UserVo();
                    userVO.setId((long) i);
                    userVO.setUserName("赵云batch" + i );
                    userVO.setHeight(1.88D);
                    userVO.setAge(10 + i);
                    userVO.setCreateTime(new Date());
                    list.add(userVO);
                }
                // 批量新增
                BulkResponse response;
                try {
                    response = documentDemoService.bulkCreate(INDEX_NAME, list);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
                List<BulkResponseItem> items = response.items();
                for (BulkResponseItem item : items) {
                    System.out.println("BulkResponseItem.toString() -> " + item.toString());
                }
            });
            futureList.add(future);
        }
        CompletableFuture.allOf(futureList.toArray(new CompletableFuture[0])).join();
    }

    @Test
    public void testGetById() throws Exception {
        long id = 46500000L;
        UserVo userVo = documentDemoService.getById(INDEX_NAME, Long.toString(id),UserVo.class);
        System.out.println("userVo ->" + userVo);

    }

    @Test
    public void testGetObjectNode() throws Exception {
        long id = 6490000L;
        ObjectNode objectNode = documentDemoService.getObjectNodeById(INDEX_NAME, Long.toString(id));

        Assertions.assertNotNull(objectNode);
        System.out.println("id ->" + objectNode.get("id").asLong());
        System.out.println("userName ->" + objectNode.get("userName").asText());
    }

}
