package com.example.es;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch.indices.CreateIndexResponse;
import co.elastic.clients.elasticsearch.indices.DeleteIndexResponse;
import co.elastic.clients.elasticsearch.indices.GetIndexResponse;
import co.elastic.clients.transport.endpoints.BooleanResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

/**
 * es客户端配置集成是否成功测试
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ESClientConfigTest {

	@Autowired
	private ElasticsearchClient client;


	/**
	 * 创建索引
	 *
	 */
	@Test
	public void createIndex() throws IOException {

		CreateIndexResponse products = client.indices().create(c -> c.index("uservo"));
		System.out.println(products.acknowledged());

	}

	/**
	 * 查询创建的索引
     */
	@Test
	public void queryIndex() throws IOException {
		GetIndexResponse getIndexResponse = client.indices().get(c->c.index("uservo"));
		System.out.println(getIndexResponse);
	}

	/**
	 * 判断索引是否存在
	 *
	 */
	@Test
	public void indexExi() throws IOException {
		BooleanResponse exists = client.indices().exists(e -> e.index("uservo"));
		System.out.println(exists.value());

	}

	/**
	 * 删除索引
	 */
	@Test
	public void deleteIndex() throws IOException{
		DeleteIndexResponse deleteIndexResponse = client.indices().delete(c->c.index("uservo"));
		System.out.println(deleteIndexResponse.acknowledged());
	}

}
