package com.example.es.entity;

import lombok.Data;

/**
 * @author hulei
 * @date 2023/8/26 19:11
 */

@Data
public class ESDocument {
    public Long id;
}
