package com.example.es.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author hulei
 * @date 2023/8/30 13:39
 */

@Data
public class PageData<T> implements Serializable {
    List<T> result;

    long total;

    public PageData(List<T> result, long total) {
        this.result = result;
        this.total = total;
    }
}
